<?php

/**
 * Application wide configuration file
 */

define('MYSQL', 'MYSQL');
define('ELASTIC_SEARCH', 'ELASTIC_SEARCH');

/**
 * Configuration of database engine
 */
define('DB_ENGINE', ELASTIC_SEARCH);
