BOOT!Q sample application
=========================

This is a sample application that implements product caching. 

Installation
------------
First, you will need to install [Composer](http://getcomposer.org/) following the instructions on their site.

Then, simply run the following command:

```sh
composer install
```

Configuration
-------------
You can define which database engine will be used by editing `app/config.php` configuration file. By default elastic search engine is used.


Testing
-------
Application can be tested by running following command:

```sh
composer run test
```
