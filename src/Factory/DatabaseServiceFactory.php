<?php

namespace App\Factory;

use App\Service\ElasticSearchService;
use App\Service\MySQLServiceAdapter;

class DatabaseServiceFactory
{
    protected $_mysqlServiceAdapter;
    protected $_elasticSearchService;

    public function __construct(MySQLServiceAdapter $mysqlServiceAdapter, ElasticSearchService $elasticSearchService)
    {
        $this->_mysqlServiceAdapter = $mysqlServiceAdapter;
        $this->_elasticSearchService = $elasticSearchService;
    }

    public function getService()
    {
        switch (DB_ENGINE) {
            case MYSQL:
                return $this->_mysqlServiceAdapter;
            case ELASTIC_SEARCH:
            default:
                return $this->_elasticSearchService;
        }
    }
};
