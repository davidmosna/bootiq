<?php

namespace App\Service;

use App\Service\MySQLService;

class MySQLServiceAdapter
{
    private $_mysqlService;

    public function __construct(MySQLService $mysqlService)
    {
        $this->_mysqlService = $mysqlService;
    }

    /**
     * This method internally uses MySQL service to query for the product
     *
     * @param [string] $id
     * 
     * @return array
     */
    public function findById($id)
    {
        return $this->_mysqlService->findProduct($id);
    }
}
