<?php

namespace App\Service;

use Exception;

class CacheService
{
    private $_cache;

    public function __construct()
    {
        $this->_cache = [];
    }

    /**
     * Checks if item is already cached
     *
     * @param [type] $id
     * 
     * @return boolean
     */
    public function exists($id)
    {
        return array_key_exists($id, $this->_cache);
    }

    /**
     * Adds items to the cache
     * 
     * @param [string] $key
     * @param [any] $value
     * 
     * @return array
     */
    public function add($key, $value)
    {
        $tmp = [];
        $tmp['value'] = $value;
        $tmp['count'] = 0;

        $this->_cache[$key] = $tmp;

        return $tmp['value'];
    }

    public function incrementCount($id)
    {
        if (!$this->exists($id)) {
            throw new Exception('Item does not exist.');
        }

        $this->_cache[$id]['count']++;
    }

    /**
     * Returns $key => $count pairs as array
     *
     * @return array
     */
    public function getPairs()
    {
        return array_map(function ($item) {
            return $item['count'];
        }, $this->_cache);
    }
}
