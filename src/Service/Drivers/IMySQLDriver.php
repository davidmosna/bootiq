<?php

namespace App\Service\Drivers;

interface IMySQLDriver
{
    /**
     * @param [string] $id
     * 
     * @return array
     */
    public function findProduct($id);
}
