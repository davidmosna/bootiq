<?php

namespace App\Service;

use App\Factory\DatabaseServiceFactory;

class DBProxy
{
    /**
     * DB service
     *
     * @var [AbstractDatabaseService]
     */
    private $_databaseService;

    /**
     * Cache
     *
     * @var [CacheService]
     */
    private $_cacheService;

    function __construct(CacheService $cacheService, DatabaseServiceFactory $databaseServiceFactory)
    {
        $this->_cacheService = $cacheService;
        $this->_databaseService = $databaseServiceFactory->getService();
    }

    /**
     * Find element or returned the cached one by id using DB service
     *
     * @param [any] $id
     * 
     * @return array
     */
    public function findById($id)
    {
        $result = null;

        if (!$this->_cacheService->exists($id)) {
            $result = $this->_cacheService->add(
                $id,
                $this->_databaseService->findById($id)
            );
        }

        $this->_cacheService->incrementCount($id);

        return $result;
    }
}
