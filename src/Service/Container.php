<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class Container
{
    public static $container;

    private function __construct()
    {
    }

    public static function init()
    {
        self::$container = new ContainerBuilder();
        self::$container->register('productController', '\App\Controller\ProductController');
        self::$container->register('productRepository', '\App\Repository\ProductRepository');
        self::$container->register('cacheService', '\App\Service\CacheService');
        self::$container->register('elasticSearchService', '\App\Service\ElasticSearchService');
        self::$container->register('mysqlService', '\App\Service\MySQLService');

        self::$container
            ->register('databaseServiceFactory', '\App\Factory\DatabaseServiceFactory')
            ->addArgument(new Reference('mysqlServiceAdapter'))
            ->addArgument(new Reference('elasticSearchService'));

        self::$container
            ->register('mysqlServiceAdapter', '\App\Service\MySQLServiceAdapter')
            ->addArgument(new Reference('mysqlService'));

        self::$container
            ->register('dbProxy', '\App\Service\DBProxy')
            ->addArgument(new Reference('cacheService'))
            ->addArgument(new Reference('databaseServiceFactory'));
    }
}

Container::init();
