<?php

namespace App\Repository;

use App\Service\DBProxy;

class ProductRepository
{
    private $_dbProxy;

    public function __construct(DBProxy $dbProxy)
    {
        $this->_dbProxy = $dbProxy;
    }

    public function findById($id)
    {
        return $this->_dbProxy->findById($id);
    }
}
