<?php

namespace App\Controller;

use App\Repository\ProductRepository;

/**
 * ProductController
 */
class ProductController
{
    protected $_productRepository;

    function __construct(ProductRepository $productRepository)
    {
        $this->_productRepository = $productRepository;
    }

    /**
     * Returns product info in JSON format
     *
     * @param  [any] $id
     * 
     * @return string
     */
    public function detail($id)
    {
        $result = $this->_productRepository->findById($id);
        return json_encode($result);
    }
}
