<?php

namespace App\Tests;

use App\Service\Container;
use PHPUnit\Framework\TestCase;

class DBProxyTest extends TestCase
{
    protected $container;

    protected function setUp(): void
    {
        $this->container = Container::$container;
    }

    /**
     * Tests adding element to cache
     *
     * @return void
     */
    public function testCache(): void
    {
        $dbProxy = $this->container->get('dbProxy');
        $cacheService =  $this->container->get('cacheService');
        $key = '__test__';

        $dbProxy->findById($key);
        $this->assertEquals(1, $cacheService->getPairs()[$key]);

        $dbProxy->findById($key);
        $dbProxy->findById($key);
        $this->assertEquals(3, $cacheService->getPairs()[$key]);
    }
}
